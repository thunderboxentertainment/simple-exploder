using UnityEngine;
using System.Collections.Generic;

public class fragmentExploder : MonoBehaviour {

    //Editor Parameters
    public bool reset;
    public bool exploding;
    [Range(0.0f, 1.0f)]
    public float explosionTime = 0;
    public float explosionDistance = 1;
    public float randomExtraDistanceMax = 1;
    public float maxRotation = 45;
    public float minRotation = 30;

	//Variables
	private List<Transform> fragments = new List<Transform>();
    private List<Vector3> startPositions = new List<Vector3>();
    private List<Vector3> startAngles = new List<Vector3>();
    private List<Vector3> directions = new List<Vector3>();
    private List<Vector3> rotations = new List<Vector3>();
    private List<float> extraDistance= new List<float>();
    private float[] randomDirections = new float[2] { -1f, 1f};

    // Use this for initialization
    void Start ()
    {
        initalise();
    }

	void OnEnable ()
	{		
	}

	// Update is called once per frame
	void Update () {
        if (exploding)
        {
            int i = 0;
            while (i<fragments.Count)
            {
                fragments[i].position = startPositions[i] + (explosionTime * (explosionDistance + extraDistance[i]) * directions[i]);
                fragments[i].eulerAngles = startAngles[i] + (explosionTime * rotations[i]);
                i++;
            }
        }

        if (reset) doReset();
    }

    public void initalise()
    {
        Transform myParent = transform.parent;
        Vector3 myWorldPos = transform.position;
        
        fragments.RemoveRange(0, fragments.Count);
        directions.RemoveRange(0, directions.Count);
        rotations.RemoveRange(0, rotations.Count);
        startAngles.RemoveRange(0, startAngles.Count);
        startPositions.RemoveRange(0, startPositions.Count);
        extraDistance.RemoveRange(0, extraDistance.Count);

        int i = 0;
        while (i<myParent.childCount)
        {
            Transform currentObj = myParent.GetChild(i);
            if (currentObj != transform)
            {
                fragments.Add(currentObj);
                startPositions.Add(currentObj.position);
                startAngles.Add(currentObj.eulerAngles);
                directions.Add(Vector3.Normalize(currentObj.position - myWorldPos));
                float angle = Random.Range(minRotation, maxRotation);
                rotations.Add(new Vector3(randomDirections[Random.Range(0,2)] * angle, randomDirections[Random.Range(0, 2)] * angle, randomDirections[Random.Range(0, 2)] * angle));
                extraDistance.Add(Random.Range(0, randomExtraDistanceMax));
            }
            i++;
        }
    }

    public void doReset()
    {
        exploding = false;
        int i = 0;
        while (i < fragments.Count)
        {
            fragments[i].position = startPositions[i];
            fragments[i].eulerAngles = startAngles[i];
            i++;
        }
        initalise();
        reset = false;
    }
}
