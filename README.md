**Simple Exploder**

This is a very simple script you can attach to an object to create an explosion effect in Unity.

- Place the script on a child in the object you want to explode - this child object will be the centre of the explosion.
- All other children objects will be blasted away from the centre.
- You will need to use your favourite tweening system to tween the value of the explosion.
- You can create pre-destroyed geometry with [this](http://www.scriptspot.com/3ds-max/scripts/fracture-voronoi) handy 3DS Max script.

Feel free to use it in your projects and give [@Thunderbox_ent](https://twitter.com/Thunderbox_ent) a shout on Twitter!